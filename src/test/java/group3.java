import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.codeborne.selenide.Selenide.*;

public class group3 {
    @BeforeClass
    public static void setUp() {
        Configuration.browser = "chrome";
        Configuration.timeout = 2000;
    }

    SelenideElement sideA = $$("td").find(Condition.text("Сторона А")).$("input");
    SelenideElement sideB = $$("td").find(Condition.text("Сторона Б")).$("input");
    SelenideElement sideC = $$("td").find(Condition.text("Сторона В")).$("input");
    SelenideElement button = $("button");
    SelenideElement result = $(".answerLabel");

@Test
public void testCaseNumber30() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("3");
    sideB.setValue("4");
    sideC.setValue("5");
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
}

@Test
public void testCaseNumber31() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("3");
    sideB.setValue("5");
    sideC.setValue("4");
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
}

@Test
public void testCaseNumber32() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("5");
    sideB.setValue("3");
    sideC.setValue("4");
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
}

@Test
public void testCaseNumber33() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("0.00000005");
    sideB.setValue("0.00000003");
    sideC.setValue("0.00000004");
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
}

@Test
public void testCaseNumber34() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("0.00000005");
    sideB.setValue("0.00000003");
    sideC.setValue("0.00000004");
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
}

@Test
public void testCaseNumber35() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("0.00000005");
    sideB.setValue("0.00000003");
    sideC.setValue("0.00000004");
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
    button.click();
    result.shouldHave(Condition.text("Прямоугольный"));
}

@Test
public void testCaseNumber36() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("-3");
    sideB.setValue("4");
    sideC.setValue("5");
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
}

@Test
public void testCaseNumber37() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("3");
    sideB.setValue("-4");
    sideC.setValue("5");
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
}

@Test
public void testCaseNumber38() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("3");
    sideB.setValue("4");
    sideC.setValue("-5");
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
}



}
