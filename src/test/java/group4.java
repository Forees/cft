import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.codeborne.selenide.Selenide.*;

public class group4 {
    @BeforeClass
    public static void setUp() {
        Configuration.browser = "chrome";
        Configuration.timeout = 2000;
    }

    SelenideElement sideA = $$("td").find(Condition.text("Сторона А")).$("input");
    SelenideElement sideB = $$("td").find(Condition.text("Сторона Б")).$("input");
    SelenideElement sideC = $$("td").find(Condition.text("Сторона В")).$("input");
    SelenideElement button = $("button");
    SelenideElement result = $(".answerLabel");

@Test
public void testCaseNumber39() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("2");
    sideB.setValue("3");
    sideC.setValue("4");
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
}

@Test
public void testCaseNumber40() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("2");
    sideB.setValue("4");
    sideC.setValue("3");
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
}

@Test
public void testCaseNumber41() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("4");
    sideB.setValue("3");
    sideC.setValue("2");
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
}

@Test
public void testCaseNumber42() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("0.00000002");
    sideB.setValue("0.00000003");
    sideC.setValue("0.00000004");
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
}

@Test
public void testCaseNumber43() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("0.00000002");
    sideB.setValue("0.00000004");
    sideC.setValue("0.00000003");
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
}

@Test
public void testCaseNumber44() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("0.00000004");
    sideB.setValue("0.00000003");
    sideC.setValue("0.00000002");
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
    button.click();
    result.shouldHave(Condition.text("Разносторонний"));
}

@Test
public void testCaseNumber45() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("1");
    sideB.setValue("2");
    sideC.setValue("3");
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
}

@Test
public void testCaseNumber46() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("2");
    sideB.setValue("3");
    sideC.setValue("1");
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
}

@Test
public void testCaseNumber47() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("3");
    sideB.setValue("2");
    sideC.setValue("1");
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
}


@Test
public void testCaseNumber48() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("-2");
    sideB.setValue("3");
    sideC.setValue("4");
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
}

@Test
public void testCaseNumber49() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("2");
    sideB.setValue("-3");
    sideC.setValue("4");
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
}

@Test
public void testCaseNumber50() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("2");
    sideB.setValue("3");
    sideC.setValue("-4");
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
}



}
