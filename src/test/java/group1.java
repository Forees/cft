import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.junit.BeforeClass;
import org.junit.Test;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class group1 {
    @BeforeClass
    public static void setUp() {
        Configuration.browser = "chrome";
        Configuration.timeout = 2000;
    }

    SelenideElement sideA = $$("td").find(Condition.text("Сторона А")).$("input");
    SelenideElement sideB = $$("td").find(Condition.text("Сторона Б")).$("input");
    SelenideElement sideC = $$("td").find(Condition.text("Сторона В")).$("input");
    SelenideElement button = $("button");
    SelenideElement result = $(".answerLabel");

@Test
public void testCaseNumber1() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?toke3n=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("1");
    sideB.setValue("1");
    sideC.setValue("1");
    button.click();
    result.shouldHave(Condition.text("Равносторонний"));
    button.click();
    result.shouldHave(Condition.text("Равносторонний"));
    button.click();
    result.shouldHave(Condition.text("Равносторонний"));
    button.click();
    result.shouldHave(Condition.text("Равносторонний"));
    button.click();
    result.shouldHave(Condition.text("Равносторонний"));
    button.click();
    result.shouldHave(Condition.text("Равносторонний"));
}

@Test
public void testCaseNumber2() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("0.00000001");
    sideB.setValue("0.00000001");
    sideC.setValue("0.00000001");
    button.click();
    result.shouldHave(Condition.text("Равносторонний"));
    button.click();
    result.shouldHave(Condition.text("Равносторонний"));
    button.click();
    result.shouldHave(Condition.text("Равносторонний"));
    button.click();
    result.shouldHave(Condition.text("Равносторонний"));
    button.click();
    result.shouldHave(Condition.text("Равносторонний"));
    button.click();
    result.shouldHave(Condition.text("Равносторонний"));
    }
@Test
public void testCaseNumber3() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("9999999999");
    sideB.setValue("9999999999");
    sideC.setValue("9999999999");
    button.click();
    result.shouldHave(Condition.text("Равносторонний"));
    button.click();
    result.shouldHave(Condition.text("Равносторонний"));
    button.click();
    result.shouldHave(Condition.text("Равносторонний"));
    button.click();
    result.shouldHave(Condition.text("Равносторонний"));
    button.click();
    result.shouldHave(Condition.text("Равносторонний"));
    button.click();
    result.shouldHave(Condition.text("Равносторонний"));
}

@Test
public void testCaseNumber4() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("0");
    sideB.setValue("0");
    sideC.setValue("0");
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
}

@Test
public void testCaseNumber5() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("-1");
    sideB.setValue("-1");
    sideC.setValue("-1");
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
}



}
