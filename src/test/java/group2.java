import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.codeborne.selenide.Selenide.*;

public class group2 {
    @BeforeClass
    public static void setUp() {
        Configuration.browser = "chrome";
        Configuration.timeout = 2000;
    }

    SelenideElement sideA = $$("td").find(Condition.text("Сторона А")).$("input");
    SelenideElement sideB = $$("td").find(Condition.text("Сторона Б")).$("input");
    SelenideElement sideC = $$("td").find(Condition.text("Сторона В")).$("input");
    SelenideElement button = $("button");
    SelenideElement result = $(".answerLabel");

@Test
public void testCaseNumber6() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("13");
    sideB.setValue("13");
    sideC.setValue("17");
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
}

@Test
public void testCaseNumber7() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("13");
    sideB.setValue("17");
    sideC.setValue("13");
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    }
@Test
public void testCaseNumber8() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("17");
    sideB.setValue("13");
    sideC.setValue("13");
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
}

@Test
public void testCaseNumber9() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("1");
    sideB.setValue("1");
    sideC.setValue("0.00000001");
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
}

@Test
public void testCaseNumber10() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("1");
    sideB.setValue("0.00000001");
    sideC.setValue("1");
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
}
@Test
public void testCaseNumber11() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("0.00000001");
    sideB.setValue("1");
    sideC.setValue("1");
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
}
@Test
public void testCaseNumber12() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("0.00000002");
    sideB.setValue("0.00000002");
    sideC.setValue("0.00000001");
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
}
@Test
public void testCaseNumber13() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("0.00000002");
    sideB.setValue("0.00000001");
    sideC.setValue("0.00000002");
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
}
@Test
public void testCaseNumber14() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("0.00000001");
    sideB.setValue("0.00000002");
    sideC.setValue("0.00000002");
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
}
@Test
public void testCaseNumber15() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("1");
    sideB.setValue("9999999999");
    sideC.setValue("9999999999");
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
}

@Test
public void testCaseNumber16() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("9999999999");
    sideB.setValue("1");
    sideC.setValue("9999999999");
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
}
@Test
public void testCaseNumber17() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("9999999999");
    sideB.setValue("9999999999");
    sideC.setValue("1");
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
}
@Test
public void testCaseNumber18() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("9999999999");
    sideB.setValue("9999999999");
    sideC.setValue("0.00000001");
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
}
@Test
public void testCaseNumber19() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("9999999999");
    sideB.setValue("0.00000001");
    sideC.setValue("9999999999");
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
}
@Test
public void testCaseNumber20() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("0.00000001");
    sideB.setValue("9999999999");
    sideC.setValue("9999999999");
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
    button.click();
    result.shouldHave(Condition.text("Равнобедренный"));
}
@Test
public void testCaseNumber21() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("0.00000002");
    sideB.setValue("0.00000001");
    sideC.setValue("0.00000001");
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
}
@Test
public void testCaseNumber22() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("0.00000001");
    sideB.setValue("0.00000002");
    sideC.setValue("0.00000001");
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
}

@Test
public void testCaseNumber23() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("0.00000001");
    sideB.setValue("0.00000001");
    sideC.setValue("0.00000002");
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
}

@Test
public void testCaseNumber24() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("10");
    sideB.setValue("10");
    sideC.setValue("30");
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
}

@Test
public void testCaseNumber25() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("10");
    sideB.setValue("30");
    sideC.setValue("10");
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
}

@Test
public void testCaseNumber26() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("30");
    sideB.setValue("10");
    sideC.setValue("10");
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
}

@Test
public void testCaseNumber27() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("-3");
    sideB.setValue("3");
    sideC.setValue("5");
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
}

@Test
public void testCaseNumber28() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("3");
    sideB.setValue("-5");
    sideC.setValue("3");
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
}

@Test
public void testCaseNumber29() {
    open("https://team.cft.ru/triangle/zadanie/triangle.html?token=05f9eca39802404e88feeee5007984b8");
    sideA.setValue("5");
    sideB.setValue("3");
    sideC.setValue("-3");
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
    button.click();
    result.shouldHave(Condition.text("Не треугольник"));
}

}
